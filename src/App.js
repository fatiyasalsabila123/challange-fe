import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import Login from './Page/Login';
import Home from './Page/Home';
import Register from './Page/Register1';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <main>
      <Switch>
          
          <Route path="/home" component={Home} exact/>
          <Route path="/register" component={Register} exact/>
          <Route path="/" component={Login} exact/>
        </Switch>
        </main>
        </BrowserRouter>
    </div>
  );
}

export default App;
